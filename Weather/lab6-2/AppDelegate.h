//
//  AppDelegate.h
//  lab6-2
//
//  Created by Иван Матяш on 4/13/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
