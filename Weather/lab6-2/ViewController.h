//
//  ViewController.h
//  lab6-2
//
//  Created by Иван Матяш on 4/13/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *indicator;
- (IBAction)refresh:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *cityBox;

@end
