//
//  Analyzer.h
//  Lab6tp
//
//  Created by Иван Матяш on 4/11/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Analyzer : NSObject
- (void)analyze:(NSString *) bar;

@end
