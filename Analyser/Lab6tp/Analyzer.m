//
//  Analyzer.m
//  Lab6tp
//
//  Created by Иван Матяш on 4/11/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

#import "Analyzer.h"

@implementation Analyzer
- (void) analyze: (NSString *)bar
{
    NSArray *words = [bar componentsSeparatedByString:@" "];                        // массив слов, разделенных пробелами
    NSMutableDictionary *statistics = [[NSMutableDictionary alloc] init];           // объявление изменяемого словаря
    for (NSString * word in words)                                                  // for-each цикл
    {
        NSNumber *repetitions = [statistics valueForKey:word];                      
        if ([repetitions integerValue] == 0)                                        // если такого слова не было, добавляем его в словарь
        {
            [statistics setObject:[[NSNumber alloc] initWithLong:1] forKey:word];
        } else
        {
            [statistics setObject:[[NSNumber alloc] initWithLong:([repetitions integerValue] + 1)]  forKey:word];    // если такое есть, прибавляем 1 к количеству
        }
    }    
    NSArray* sortedMas = [statistics keysSortedByValueUsingComparator:^(id obj1, id obj2) {                          // сортировка словаря (с помощью компаратора)
        int v1 = [obj1 intValue];
        int v2 = [obj2 intValue];
        if (v1 > v2)
            return NSOrderedAscending;
        else if (v1 < v2)
            return NSOrderedDescending;
        else
            return NSOrderedSame;
    }];                                                                             // счетчик выведенных слов    
    int count = 0;
    for(NSString* amount in sortedMas)
    {
        NSNumber *num = [statistics valueForKey : amount];
        NSLog(@"%@: %ld", amount, (long)[num integerValue]);                       // вывод найденных слов
        if (++count > 4)
            break;
    }
}

@end
