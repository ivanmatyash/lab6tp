//
//  main.m
//  Lab6tp
//
//  Created by Иван Матяш on 4/11/16.
//  Copyright (c) 2016 Иван Матяш. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Analyzer.h"

int main(int argc, const char * argv[])
{
    Analyzer *analyser = [[Analyzer alloc] init];
    
    [analyser analyze:@"Hello World"];
    
    return 0;
}

